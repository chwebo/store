
# 导包、远程电脑地址、传参appium、录制脚本、停止
from  appium  import webdriver
from appium.webdriver.common.touch_action import  TouchAction
import time
# 电脑地址
url = "127.0.0.1:4723/wd/hub"
# appium传入参数
param = {
  "deviceName": "3126f039",
  "platformName": "Android",
  "platformVersion": "10",
  "appPackage": "com.ss.android.ugc.aweme",
  "appActivity": "com.ss.android.ugc.aweme.splash.SplashActivity"
}
# 将真机与电脑连接
driver = webdriver.Remote(url,param)
time.sleep(5)
TouchAction(driver).tap(x=545, y=1493).perform()
time.sleep(5)
TouchAction(driver).tap(x=581, y=1274).perform()
time.sleep(5)
TouchAction(driver).press(x=469, y=1538).move_to(x=525, y=571).release().perform()
time.sleep(5)
TouchAction(driver).tap(x=983, y=143).perform()
time.sleep(5)
el1 = driver.find_element_by_id("com.ss.android.ugc.aweme:id/et_search_kw")
time.sleep(5)
el1.send_keys("篮球")
time.sleep(5)
TouchAction(driver).tap(x=968, y=143).perform()
time.sleep(5)
TouchAction(driver).tap(x=255, y=504).perform()
time.sleep(5)
TouchAction(driver).press(x=565, y=1880).move_to(x=550, y=810).release().perform()
time.sleep(5)
TouchAction(driver).tap(x=535, y=1162).perform()
time.sleep(5)
TouchAction(driver).tap(x=979, y=1327).perform()
time.sleep(5)
TouchAction(driver).tap(x=627, y=1062).perform()
time.sleep(5)
TouchAction(driver).tap(x=577, y=1446).perform()
time.sleep(5)
TouchAction(driver).tap(x=975, y=151).perform()
time.sleep(5)
# 退出
driver.quit()

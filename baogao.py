
from HTMLTestRunner import HTMLTestRunner
import unittest

# 运行生成测试报告
tests=unittest.defaultTestLoader.discover(r"E:\pythonProject",pattern="Test*.py")
runner = HTMLTestRunner.HTMLTestRunner(
    title = "社保系统测试报告",
    description = "查询功能测试报告",
    verbosity = 1,
    stream = open(file="shebao.html",mode="w+",encoding='utf-8')
)
# 执行用例
runner.run(tests)

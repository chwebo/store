

class InitPage:
    loginSuccessData = [
        {'username':'a','password':'b','expect':'stulogin'},
        {'username': 'c', 'password': 'd','expect':'stulogin'}
    ]

    loginErrorData = [
        {'username': 'a1', 'password': 'b', 'expect': '用户名或密码错误'},
        {'username': 'c', 'password': 'd1', 'expect': '用户名或密码错误'}
    ]

from ddt import ddt
from ddt import data
from selenium import webdriver
from unittest import TestCase

from zb import InitPage
from lj import logino

@ddt

class TestLogin(TestCase):
    @data(*InitPage.loginSuccessData)
    def testLoginSuccess(self,testdata):
        # 引入数据
        username = testdata['username']
        pwd = testdata['password']
        expect =testdata['expect']
        #执行测试
        driver = webdriver.Chrome()
        driver.get('sdfs')
        driver.maximize_window()

        login = logino(driver)
        login.login(username,pwd)
        # 提取实际结果
        result = login.getSuccessResult()
        driver.quit()
        # 断言
        self.assertEqual(result,expect)
    # 登陆失败的测试
    @data(*InitPage.loginErrorData)
    def testLoginError(self, testdata):
            # 引入数据
            username = testdata['username']
            pwd = testdata['password']
            expect = testdata['expect']
            # 执行测试
            driver = webdriver.Chrome()
            driver.get('sdfs')
            driver.maximize_window()

            login = logino(driver)
            login.login(username, pwd)
            # 提取实际结果
            result = login.getErrorResult()
            driver.quit()
            # 断言
            self.assertEqual(result, expect)
from ddt import ddt
from ddt import data
from selenium import webdriver
from unittest import TestCase
from chaxunzhunbei import chaxunzhunbei
from chaxunluoji import chaxunluoji

@ddt
class Testchaxun(TestCase):
    # 查询成功用例
    @data(*chaxunzhunbei.cha_success_data)
    def testSuccessCase(self,testdata):
        cname = testdata['placeholder']
        expect = testdata['expect']

#         打开浏览器
        driver = webdriver.Chrome()
        driver.get('http://ehrsoc.ibizlab.cn/')
        driver.maximize_window()
        driver.find_element_by_xpath('//*[@id="app"]/div[2]/div/div[1]/div[2]/div/form/div[3]/div/button[1]').click()

        luoji = chaxunluoji(driver)
        luoji.chaxun(cname)
#         获取实际结果
        result  = luoji.getSuccessData()
        self.assertEqual(result,expect)
        driver.quit()


#         查询失败用例
    @data(*chaxunzhunbei.cha_error_data)
    def testErrorCase(self, testdata):
        cname = testdata['placeholder']
        expect = testdata['expect']

        #         打开浏览器
        driver = webdriver.Chrome()
        driver.get('http://ehrsoc.ibizlab.cn/')
        driver.maximize_window()
        driver.find_element_by_xpath(
            '//*[@id="app"]/div[2]/div/div[1]/div[2]/div/form/div[3]/div/button[1]').click()

        luoji = chaxunluoji(driver)
        luoji.chaxun(cname)
        #         获取实际结果
        result = luoji.getErrorData()
        self.assertEqual(result, expect)
        driver.quit()

















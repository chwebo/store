

import unittest
from Calc import Calc
class TestCalc(unittest.TestCase):

    def testAdd(self):
        # 准备数据
        a = 1
        b = 2
        c = 3
        # 调用被测程序
        calc = Calc()
        sum = calc.add(a,b)
#         断言:将预期结果与实际结果进行比较
        self.assertEqual(c,sum)

    def testAdd1(self):
            # 准备数据
            a = -1
            b = 2
            c = 4
            # 调用被测程序
            calc = Calc()
            sum = calc.add(a, b)
            # 断言:将预期结果与实际结果进行比较
            self.assertEqual(c, sum)

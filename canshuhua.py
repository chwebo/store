# 参数化测试
import unittest
from unittest import TestCase

from ddt import ddt
from ddt import data
from ddt import unpack

class Calc:
    def add(self,a,b):
        return a+b
# 数据源
da = [
    [1,2,3],
    [2,3,5],
    [-1,-2,-3]
]

# 参数化类，用来修饰类
@ddt
class TestCalc(TestCase):
    # 引入数据源
    @data(*da)
    # 将引入的数据源进行解包
    @unpack
    def testAdd(self,a,b,c):
        calc= Calc()
        sum=calc.add(a,b)
#         断言，将预期结果sum与实际结果c进行比较
        self.assertEqual(sum,c)


